# Cajero

_Test de desarrollo_

## Requerimientos 

_Docker 18.09.2_
_Docker-compose 1.23.2_

### Instalación

_A continuación estan las instrucciones en un ambiente local_

_terminal  run_

```
$ sudo git clone ...
```

_cd [path]/cajero_

_terminal  run_

```
$ sudo chmod -R 777 examen
```

_cd [path]/cajero/examen_

_terminal  run_

```
$ cp .env.example .env
```

_terminal  run_

```
$ composer install
```

_cd [path]/cajero/examen/laradock_

_terminal  run_

```
$ cp env-example .env
```

_terminal  run_

```
$ sudo docker-compose up mysql nginx
```

_abre nueva terminal_

_cd  [path]/ cajero/examen/laradock_

_terminal  run_

```
$ sudo docker-compose exec --user=laradock  workspace bash
```

_Crea una base de datos llamada cajero dentro de mysql usuario: root contraseña:root_

_terminal  run_

```
: /var/www$ php artisan migrate
```

_terminal  run_

```
: /var/www$ php artisan db:seed
```

### Credenciales 

_Las credenciales para ingresar al sistema son: _

_Rol administrador_
	_usr: admin@admin.com_
	_pass: secret_
	
_Rol cliente_
	_usr: cliente@cliente.com_
	_pass: secret_


## Ejecutando las pruebas 

_Se realizaron pruebas unitarias solamente de las url de api_

```
/var/www$ vendor/bin/phpunit
```

## Construido con 

* [Laravel 5.6](https://laravel.com/) - El framework web usado
* [Mysql](https://www.mysql.com/) - Manejador de DB
* [Docker](https://www.docker.com/) - Contenedor

## Autores 

* **Diego Hernández Moreno** - 
