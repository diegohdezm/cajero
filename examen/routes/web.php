<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/denied', ['as' => 'denied', function() {
    return view('partials.unauthorized');
}]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout'); // Get logout hack

//Route::get('/cuentabientes', 'Admin\UsuarioController@index')->name('usuario.index');
//Route::get('/cuentabientes/crea', 'Admin\UsuarioController@create')->name('usuario.create');
//Route::get('/cuentabientes/{id}', 'Admin\UsuarioController@show')->name('usuario.show');

// Admin
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    require base_path('routes/web/admin/admin.php');
});

// Clientes
Route::group(['namespace' => 'Sistema', 'prefix' => 'admin'], function () {
    require base_path('routes/web/admin/cliente/cliente.php');
});