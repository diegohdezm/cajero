<?php


// Comisiones
Route::get('/comisiones', 'ComisionesController@index')->name('comision.index');
Route::get('/comisiones/crea', 'ComisionesController@create')->name('comision.create');
Route::get('/comisiones/{id}', 'ComisionesController@show')->name('comision.show');
Route::post('/comisiones/guarda', 'ComisionesController@store')->name('comision.store');
Route::get('/comisiones/delete/{id}', 'ComisionesController@destroy')->name('comision.delete');

// Estados
Route::get('/estados', 'EstadoController@index')->name('estado.index');
Route::get('/estados/crea', 'EstadoController@create')->name('estado.create');
Route::get('/estados/{id}', 'EstadoController@show')->name('estado.show');
Route::post('/estados/guarda', 'EstadoController@store')->name('estado.store');
Route::get('/estados/delete/{id}', 'EstadoController@destroy')->name('estado.delete');