<?php


// Usuarios
Route::get('/cuentabientes', 'UsuarioController@index')->name('usuario.index');
Route::get('/cuentabientes/crea', 'UsuarioController@create')->name('usuario.create');
Route::get('/cuentabientes/{id}', 'UsuarioController@show')->name('usuario.show');
Route::post('/cuentabientes/guarda', 'UsuarioController@store')->name('usuario.store');

//Cuentas
Route::get('/cuentabientes/cuenta/crea/{id}', 'CuentaController@create')->name('cuenta.create');
Route::post('/cuentabientes/cuenta/guarda/{id}', 'CuentaController@store')->name('cuenta.store');