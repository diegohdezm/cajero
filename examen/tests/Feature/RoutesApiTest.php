<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoutesApiTest extends TestCase
{

    /**
     * Test ruta comisiones api.
     *
     * @return void
     */
    public function testCuentahabientesApi()
    {
        $this->get('/api/cuentabientes')
            ->assertStatus(200);
    }

    /**
     * Test ruta comisiones api.
     *
     * @return void
     */
    public function testComisionesApi()
    {
        $this->get('/api/comisiones')
            ->assertStatus(200);
    }

    /**
     * Test ruta estados api.
     *
     * @return void
     */
    public function testEstadosApi()
    {
        $this->get('/api/estados')
            ->assertStatus(200);
    }
}
