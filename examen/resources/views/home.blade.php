@extends('layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content --> 
  <div class="content-wrapper" style="margin-top:-20px"> 
    <!-- Content Header (Page header) --> 
    <section class="content-header"> 
      <h1 style="margin-top:-20px"> Bienvenido <small> {{Auth::user()->nombre}}</small> </h1>  
    </section>  
    <!-- Main content --> 
    <section class="content">  
      <!-- Default box --> 
      <div class="box" style="margin-top:-50px"> 
        <div class="box-header with-border"> 
          <h3 class="box-title">TEST</h3>  
        </div> 
        <div class="box-body"> Test backend Diego Hernandez Moreno</div> 
        <!-- /.box-body --> 
        <!-- /.box-footer--> 
      </div> 
      <!-- /.box -->  
    </section> 
    <!-- /.content --> 
  </div> 
  <!-- /.content-wrapper -->

@endsection