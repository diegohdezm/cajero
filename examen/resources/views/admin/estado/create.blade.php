@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top:0px"><br>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="margin-top:-20px"> Estado<small> Comision</small> </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                    <!-- Formulario -->
                        <form role="form" method="POST" action="{{ route('estado.store') }}">
                            {{ csrf_field() }}
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Ingresa los datos para crear un estado</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="">Estado</label>
                                        <input type="text" class="form-control" id="estado" name="estado" placeholder="Ingresa el estado">
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success">Registrar</button>
                                    </div>
                                </div>
                        </form>
                        <!-- /. Formulario -->
                    </div>
                </section>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection