@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top:0px"><br>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="margin-top:-20px"> Detalle <small>Estado</small> </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                    <!-- Formulario -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Comision</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                                <i class="fa fa-minus"></i></button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                                <i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <h4>Estado</h4>
                                        <p>Nombre del estado: <strong>{{ $estado->estado }}</strong></p>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger"><i class="fa fa-trash"></i> Elimina</button>
                                    </div>
                                    <!-- /.box-footer-->
                                </div>
                            </div>
                        </div>
                        <!-- /. Formulario -->
                    </div>
                </section>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal modal-danger fade" id="modal-danger" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Advertencia</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estas seguro de borrar la comision?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                    <a href="{{ route('comision.delete', ['id' => $estado->id]) }}" role="button" class="btn btn-outline"><i class="fa fa-trash"></i> Borrar</a>
                </div>
            </div>
        </div>
    </div>

@endsection