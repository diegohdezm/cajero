@extends('layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top:0px"><br>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="margin-top:-20px"> Cuentahabientes<small>Listado</small> </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Cuentahabientes</h3>
                            <div class="box-tools">
                                <a href="{{ route('usuario.create') }}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Registra cuentahabiente</a>
                            </div>
                        </div>
                        <div class="box-body" id="table1">
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Apellido Materno</th>
                                    <th>Correo</th>
                                    <th>Edad</th>
                                    <th>Antiguedad</th>
                                    <th>Detalles</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </section>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@section('javascript')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        jQuery(function($){
            // Inicializa tabla de datos
            var dtable = $('#example').DataTable({
                "ajax": {
                    "url": "/api/cuentabientes",
                    "dataSrc": "data.usuarios.data",
                    "data": function (d) {
                        var request_data = {};
                        request_data.per_page = d.length;
                        request_data.page = Math.ceil(d.start / d.length) + 1;
                        request_data.order = d.columns[d.order[0].column].data;
                        request_data.sort = d.order[0].dir;
                        request_data.search = d.search.value;
                        return request_data;

                    },
                    "dataFilter": function(response_data){
                        var d = jQuery.parseJSON(response_data);
                        d.recordsTotal = d.data.usuarios.total;
                        d.recordsFiltered = d.data.usuarios.total;
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {data: 'nombre'},
                    {data: 'a_paterno'},
                    {data: 'a_materno'},
                    {data: 'email'},
                    {data: 'edad'},
                    {data: 'created_at', render: function (d) { if(d) { return d.slice(" ", 10) } else { return null } } },
                    {data: null, orderable: false, render: function (d) { return '<a href="{{ url('/admin/cuentabientes') }}/' + d.id + '" class="btn btn-primary btn-xs" role="button"><i class="fa fa-eye"></i> Detalles</a>'; } }
                ],
                // Opciones iguales en todas las tablas.
                "pageLength": 25,
                "serverSide": true,
                "searchDelay": 1500,
                @include('admin/partials/datatables_lang')
            });
            // Solo envía el filtro al darle "enter".
            // Corrige error de dataTables que envía el primer caracter como búsqueda al servidor.
            $("#table1 div.dataTables_filter input").unbind();
            $("#table1 div.dataTables_filter input").keyup(function (e) {
                if (e.keyCode == 13) {
                    dtable.search( this.value ).draw();
                }
            });
        });
    </script>
@stop

@endsection