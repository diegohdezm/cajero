<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo',
        'nombre',
        'a_paterno',
        'a_materno',
        'email',
        'edad',
        'status',
        'password',
        // Foranea
        'direccion_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Relaciones ===========================================
     */

    /**
     * Direccion del cliente (direccion_id).
     */
    public function direccion()
    {
        return $this->belongsTo('App\Models\Direccion', 'direccion_id');
    }
}
