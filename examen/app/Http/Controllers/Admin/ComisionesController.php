<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ComisionRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Prologue\Alerts\Facades\Alert;
use App\Models\Comision;
use Log;
use Validator;

class ComisionesController extends Controller
{
    private $mComision;

    public function __construct(Comision $comision)
    {
        $this->middleware('auth');
        $this->mComision = $comision;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Muestra plantilla de listado de tipos de cuentas
        return view('admin/comision/index')->with(['alerts' => Alert::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Muestra plantilla
        return view('admin/comision/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComisionRequest $request)
    {
        try {
            // Crea comision
            $this->mComision->create($request->all());

            return redirect()->route('comision.index');
        } catch (\Exception $e) {
            Alert::error("No se puede crear la comision. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Muestra el objeto solicitado
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                throw new HttpResponseException(redirect()->back()->withErrors($oValidator));
            }
            // Busca catalodo de comisiones
            $oComision = $this->mComision->find($id);
            if ($oComision == null) {
                return view('admin/errores/no_encontrado');
            } else {
                // Muestra plantilla
                return view('admin/comision/show')->with(['comision' => $oComision, 'alerts' => Alert::all()]);
            }
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca comision
            $oComision = $this->mComision->find($id);
            if ($oComision == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Comision', 'id' => $id]);
            }
            // Muestra plantilla
            return view('admin/comision/edita')->with(['comision' => $oComision, 'alerts' => Alert::all()]);
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComisionRequest $request, $id)
    {
        try {
            // Busca comision
            $oComision = $this->mComision->find($id);
            if ($oComision == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Estado', 'id' => $id]);
            }
            // Actualiza estado
            $oComision->update($request->all());
            Alert::success('Datos actualizados correctamente')->flash();
            return redirect()->route('comision.show', ['id' => $id]);
        } catch (\Exception $e) {
            Alert::error("No se puede actualizar el tipo de cuenta. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->route('comision.edit', ['id' => $id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                throw new HttpResponseException(redirect()->back()->withErrors($oValidator));
            }
            // Busca comision
            $oComision = $this->mComision->find($id);
            if ($oComision == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Comision', 'id' => $id]);
            }
            // Elimina comision
            $oComision->delete();
            return redirect()->route('comision.index');
        } catch (\Exception $e) {
            Alert::error("No se puede eliminar la comision. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }
}
