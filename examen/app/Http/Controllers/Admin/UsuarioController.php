<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\UsuarioRequest;
use App\Models\Cuenta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Prologue\Alerts\Facades\Alert;
use App\User;
use App\Models\Direccion;
use App\Models\Estado;
use App\Models\Telefono;
use Log;
use Validator;

class UsuarioController extends Controller
{
    private $mUsuario;
    private $mDireccion;
    private $mTelefono;
    private $mEstado;
    private $mCuenta;

    public function __construct(User $usuario, Direccion $direccion, Estado $estado, Telefono $telefono, Cuenta $cuenta)
    {
        $this->middleware('auth');
        $this->mUsuario = $usuario;
        $this->mDireccion = $direccion;
        $this->mTelefono = $telefono;
        $this->mEstado = $estado;
        $this->mCuenta = $cuenta;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Muestra plantilla de listado de usuarios
        return view('admin/usuario/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Muestra plantilla
        return view('admin/usuario/crea')->with(['alerts' => Alert::all(), 'estados' => Estado::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioRequest $request)
    {
        try {
            // Crea telefono
            $telefono = $this->mTelefono->create([
                'extencion' => $request->extencion,
                'prefijo' => $request->prefijo,
                'numero' => $request->numero
            ]);
            // Crea direccion
            $direccion = $this->mDireccion->create([
                'calle' => $request->calle,
                'cp' => $request->cp,
                'colonia' => $request->colonia,
                'telefono_id' => $telefono->id,
                'estado_id' => $request->estado_id
            ]);
            // Crea usuario
            $cuentabiente = $this->mUsuario->create([
                'nombre' => $request->nombre,
                'a_paterno' => $request->a_paterno,
                'a_materno' => $request->a_materno,
                'tipo' => 'cliente',
                'email' => $request->correo_electronico,
                'edad' => $request->edad,
                'status' => 1,
                'password' => bcrypt($request->password),
                'direccion_id' => $direccion->id
            ]);

            return redirect()->route('usuario.index');

        } catch (\Exception $e) {
            Alert::error("No se puede crear el usuario. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Muestra el objeto solicitado
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca usuario
            $oCuentabiente = $this->mUsuario->find($id);
            // Busca sus cuentas
            $oCuentas = $this->mCuenta->where('usuario_id',$id)->get();
            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            } else {
                // Muestra plantilla
                return view('admin/usuario/detalle')->with(['usuario' => $oCuentabiente,'cuentas' => $oCuentas, 'alerts' => Alert::all()]);
            }
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca usuario
            $oCuentabiente= $this->mUsuario->withTrashed()->find($id);
            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            }
            // Muestra plantilla
            return view('admin/usuario/edita')->with(['usuario' => $oCuentabiente, 'alerts' => Alert::all()]);
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioRequest $request, $id)
    {
        try {
            // Busca usuario
            $oCuentabiente = $this->mUsuario->withTrashed()->find($id);

            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            }
            // Actualiza usuario
            $oCuentabiente->nombre = $request->nombre;
            $oCuentabiente->a_paterno = $request->a_paterno;
            $oCuentabiente->a_materno = $request->a_materno;
            $oCuentabiente->correo_electronico = $request->correo_electronico;
            $oCuentabiente->edad = $request->edad;
            $oCuentabiente->status = $request->status;
            $oCuentabiente->password = $request->password;
            $oCuentabiente->save();


            Alert::success('Datos actualizados correctamente')->flash();
            return redirect()->route('usuario.show', ['id' => $id]);
        } catch (\Exception $e) {
            Alert::error("No se puede actualizar el cuentabiente. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->route('usuario.edit', ['id' => $id]);
        }
    }

    /**
     * Borra el modelo
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca usuario
            $oCuentabiente = $this->mUsuario->find($id);
            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            }
            // Borra usuario
            $oCuentabiente->delete();
            Alert::success('Cuentabiente borrado exitosamente.')->flash();
            return redirect()->back();
        } catch (\Exception $e) {
            Alert::error("No se puede borrar el cuentabiente. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Restaura el modelo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca usuario
            $oCuentabiente = $this->mUsuario->onlyTrashed()->find($id);
            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            }
            // Restaura usurio
            $oCuentabiente->restore();
            Alert::success('Cuentabiente restaurado exitosamente.')->flash();
            return redirect()->route('usuario.show', ['id' => $id]);
        } catch (\Exception $e) {
            Alert::error("No se puede restaurar el cuentabiente. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca usuario
            $oCuentabiente = $this->mUsuario->onlyTrashed()->find($id);
            if ($oCuentabiente == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'User', 'id' => $id]);
            }
            // Elimina usuario
            $oCuentabiente->forceDelete();
            Alert::success('Cuentabiente eliminado exitosamente.')->flash();
            return redirect()->route('usuario.index');
        } catch (\Exception $e) {
            Alert::error("No se puede eliminar el cuentabiente. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }


}
