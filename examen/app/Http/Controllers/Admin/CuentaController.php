<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CuentaRequest;
use Illuminate\Support\Str;
use App\Models\Comision;
use App\Models\TipoCuenta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Prologue\Alerts\Facades\Alert;
use App\Models\Cuenta;
use App\User;
use Log;
use Validator;

class CuentaController extends Controller
{
    private $mCuenta;
    private $mUsuario;
    private $mTipoCienta;
    private $mComision;

    public function __construct(Cuenta $cuenta, User $usuario, TipoCuenta $tipo_cuenta, Comision $comision)
    {
        $this->middleware('auth');
        $this->mCuenta = $cuenta;
        $this->mUsuario = $usuario;
        $this->mTipoCienta = $tipo_cuenta;
        $this->mComision = $comision;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($usuario)
    {
        $oCuentabiente = $this->mUsuario->find($usuario);
        // Muestra plantilla
        return view('admin/cuenta/crea')->with(['usuario' => $oCuentabiente, 'tipo_cuenta' => TipoCuenta::all(), 'comisiones' => Comision::all(), 'alerts' => Alert::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CuentaRequest $request, $usuario_id)
    {
        try {
            // Parsea la cantidad de credito y crdito aprobado
            $tipo_cuenta = $this->mTipoCienta->where('id',$request->tipo_cuenta_id)->get();
            foreach ($tipo_cuenta as $tCuenta) {
                $tCuenta->tipo_cuenta == 'credito' ? $credito_saldo = $request->credito_aprobado : $credito_saldo = $request->credito_saldo;
            }

            // Crea cuenta
            $cuenta = $this->mCuenta->create([
                'no_cuenta' => Str::random(20),
                'credito_saldo' => $credito_saldo,
                'credito_aprobado' => $request->credito_aprobado,
                'fecha_corte' =>  date('d'),
                'tipo_cuenta_id' => $request->tipo_cuenta_id,
                'comisiones_id' => $request->comision_id,
                'usuario_id' => $usuario_id
            ]);

            return redirect()->route('usuario.show', ['id' => $usuario_id]);
        } catch (\Exception $e) {
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($usuario_id)
    {
        // Muestra el objeto solicitado
        try {
            $oValidator = Validator::make(['usuario_id' => $usuario_id], [
                'usuario_id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca cuenta
            $oCuenta = $this->mCuenta->where('usuario_id',$usuario_id)->get();
            if ($oCuenta == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Cuenta', 'id' => $usuario_id]);
            } else {
                // Muestra plantilla
                return view('admin/cuenta/detalle')->with(['cuenta' => $oCuenta, 'alerts' => Alert::all()]);
            }
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CuentaRequest $request, $id, $usuario_id)
    {
        try {
            // Busca cuenta
            $oCuenta = $this->mCuenta->find($id);
            if ($oCuenta == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Cuenta', 'id' => $id]);
            }
            // Actualiza estado
            $oCuenta->update($request->all());
            Alert::success('Datos actualizados correctamente')->flash();
            return redirect()->route('usuario.show', ['id' => $usuario_id]);
        } catch (\Exception $e) {
            Alert::error("No se puede actualizar la cuenta. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->route('cuenta.show', ['id' => $id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                Alert::error($oValidator->errors())->flash();
                return redirect()->back()->withInput();
            }
            // Busca cuenta
            $oCuenta = $this->mCuenta()->find($id);
            if ($oCuenta == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Cuenta', 'id' => $id]);
            }
            // Elimina estado
            $oCuenta->delete();
            Alert::success('Cuenta eliminada exitosamente.')->flash();
            return redirect()->route('usuario.index');
        } catch (\Exception $e) {
            Alert::error("No se puede eliminar la cuenta. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }
    }
}
