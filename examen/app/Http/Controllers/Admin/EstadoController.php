<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EstadoRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Prologue\Alerts\Facades\Alert;
use App\Models\Estado;
use Log;
use Validator;


class EstadoController extends Controller
{
    private $mEstado;

    public function __construct(Estado $estado)
    {
        $this->middleware('auth');
        $this->mEstado = $estado;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Muestra plantilla de listado de estados
        return view('admin/estado/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Muestra plantilla
        return view('admin.estado.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstadoRequest $request)
    {
        try {
            // Crea estado
            $this->mEstado->create($request->all());
            return redirect()->route('estado.index');
        } catch (\Exception $e) {
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Muestra el objeto solicitado
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                throw new HttpResponseException(redirect()->back()->withErrors($oValidator));
            }
            // Busca catalodo de estados
            $oEstado = $this->mEstado->find($id);
            if ($oEstado == null) {
                return view('admin/errores/no_encontrado');
            } else {
                // Muestra plantilla
                return view('admin/estado/show')->with(['estado' => $oEstado, 'alerts' => Alert::all()]);
            }
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                throw new HttpResponseException(redirect()->back()->withErrors($oValidator));
            }
            // Busca estado
            $oEstado = $this->mEstado->find($id);
            if ($oEstado == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Estado', 'id' => $id]);
            }
            // Muestra plantilla
            return view('admin/estado/edita')->with(['estados' => $oEstado, 'alerts' => Alert::all()]);
        } catch (\Exception $e) {
            // Registra error
            Log::error('Error en ' . __METHOD__ . ' línea ' . $e->getLine() . ':' . $e->getMessage());
            // Muestra plantilla de error
            return view('admin/errores/excepcion')->with(['exception' => $e]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstadoRequest $request, $id)
    {
        try {
            // Busca estado
            $oEstado = $this->mEstado->find($id);
            if ($oEstado == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Estado', 'id' => $id]);
            }
            // Actualiza estado
            $oEstado->update($request->all());
            Alert::success('Datos actualizados correctamente')->flash();
            return redirect()->route('estado.show', ['id' => $id]);
        } catch (\Exception $e) {
            Alert::error("No se puede actualizar el estado. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->route('estado.edit', ['id' => $id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $oValidator = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ]);
            if ($oValidator->fails()) {
                throw new HttpResponseException(redirect()->back()->withErrors($oValidator));
            }
            // Busca estado
            $oEstado = $this->mEstado->find($id);
            if ($oEstado == null) {
                return view('admin/errores/no_encontrado')->with(['model' => 'Estado', 'id' => $id]);
            }
            // Elimina estado
            $oEstado->delete();
            Alert::success('Estado eliminado exitosamente.')->flash();
            return redirect()->route('estado.index');
        } catch (\Exception $e) {
            Alert::error("No se puede eliminar el estado. Error: " . $e->getMessage())->flash();
            Log::error('Error on ' . __METHOD__ . ' line ' . $e->getLine() . ':' . $e->getMessage());
            return redirect()->back();
        }

    }
}
