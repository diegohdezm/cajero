<?php

namespace App\Http\Controllers\Sistema;

use App\Http\Requests\CargoRequest;
use App\Http\Requests\DepositoRequest;
use App\Models\Comision;
use App\Models\Transaccion;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Cuenta;


class CargoController extends Controller
{
    private $mUsuario;
    private $mCuenta;
    private $mComision;

    /**
     * CargoController constructor.
     * @param User $usuario
     * @param Cuenta $cuenta
     * @param Transaccion $transaccion
     * @param Comision $comision
     */
    public function __construct(User $usuario, Cuenta $cuenta, Transaccion $transaccion, Comision $comision)
    {
        $this->middleware('auth');
        $this->mUsuario = $usuario;
        $this->mCuenta = $cuenta;
        $this->mTransaccion = $transaccion;
        $this->mComision = $comision;
    }

    /**
     * @param $usuario_id
     * @return $this
     */
    public function listarCuentas($usuario_id)
    {
        // Obtiene la cuenta
        $oCuentas = $this->mCuenta->where('usuario_id', $usuario_id)->get();

        return view('cliente.cuentas')->with(['cuentas' => $oCuentas]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function detalleCuenta($id)
    {
        // Obtiene la cuenta
        $oCuenta = $this->mCuenta->where('id', $id)->get();
        // Obtiene las transacciones de la cuenta
        $oTransacciones = $this->mTransaccion->where('cuenta_id', $id)->get();

        return view('cliente.detalle_cuenta')->with(['cuenta' => $oCuenta, 'transacciones' => $oTransacciones]);
    }

    /**
     * @param $id
     * @param CargoRequest $oRequest
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function cargo($id, CargoRequest $oRequest)
    {
        // Obtiene la cuenta con la comision
        $cuenta = $this->getCuenta($id);
        // Recorre la consulta
        foreach ($cuenta as $datosCuenta) {
            // Calcula la comision y la suma al retiro
            $retiro = $oRequest->retiro + ($oRequest->retiro*($datosCuenta->comision_retiro / 100));
            // Validacion de saldo a retirar
            if (($datosCuenta->credito_saldo - $retiro) < 0) {
                return redirect()->back()->withErrors('No proceso el retiro debido a saldo insuficiente');
            } else {
                // Registra transaccion
                $data = (object)[
                    'movimiento' => 'retiro',
                    'cantidad' => $retiro,
                ];
                $transaccion = $this->registraTransaccion($data, $datosCuenta->id, $datosCuenta->usuario_id);
                // Actualiza saldo
                $total = $datosCuenta->credito_saldo - $retiro;
                $actualizaCuenta = $this->actualizaSaldo($total, $id);

                return redirect()->route('cliente.detalle_cuenta', ['id' => $datosCuenta->id]);
            }
        }
    }

    /**
     * @param $id
     * @param DepositoRequest $oRequest
     * @return $this
     */
    public function depositoPago($id, DepositoRequest $oRequest)
    {
        // Obtiene la cuenta
        $cuenta = $this->getCuenta($id);
        foreach ($cuenta as $datosCuenta) {
            if (($datosCuenta->credito_aprobado - $datosCuenta->credito_saldo) < $oRequest->deposito && $datosCuenta->tipo_cuenta == 'credito') {
                return redirect()->back()->withErrors('El deposito sobrepasa el total a pagar');
            } else {
                // Registra transaccion
                $data = (object)[
                   'movimiento' =>  'deposito',
                   'cantidad' => $oRequest->deposito
                ];
                $transaccion = $this->registraTransaccion($data, $datosCuenta->id, $datosCuenta->usuario_id);
                // Actualiza saldo
                $total = $datosCuenta->credito_saldo + $oRequest->deposito;
                $actualizaCuenta = $this->actualizaSaldo($total, $id);

                return redirect()->route('cliente.detalle_cuenta', ['id' => $datosCuenta->id]);
            }
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getCuenta($id)
    {
        // Obtiene la cuenta
        $cuenta =  $this->mCuenta->where('cuenta.id', $id)
            ->leftJoin('tpo_cuenta', 'tpo_cuenta.id', '=', 'cuenta.tipo_cuenta_id')
            ->leftJoin('comisiones', 'comisiones.id', '=', 'cuenta.comisiones_id')
            ->select('cuenta.*', 'comisiones.comision_retiro', 'tpo_cuenta.tipo_cuenta')->get();

        return $cuenta;
    }

    /**
     * @param $data
     * @param $cuenta_id
     * @param $usuario_id
     * @return mixed
     */
    private function registraTransaccion($data, $cuenta_id, $usuario_id)
    {
        // Crea la transaccion
        $transaccion = $this->mTransaccion->create([
            'movimiento' => $data->movimiento,
            'cantidad' => $data->cantidad,
            'usuario_id' => $usuario_id,
            'cuenta_id' => $cuenta_id
        ]);

        return $transaccion;
    }

    /**
     * @param $total
     * @param $id
     * @return mixed
     */
    private function actualizaSaldo($total, $id)
    {
        // Busca cuenta y actualiza
        $actualizaCuenta = $this->mCuenta->find($id);
        $actualizaCuenta->credito_saldo = $total;
        $actualizaCuenta->save();

        return $actualizaCuenta;
    }
}
