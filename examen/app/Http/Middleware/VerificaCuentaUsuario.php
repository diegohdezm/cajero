<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Cuenta;
use Illuminate\Support\Facades\Auth;

class VerificaCuentaUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cuenta_id = Cuenta::where('id',$request->id)->select('cuenta.usuario_id')->get();
        foreach ($cuenta_id as $id) {
            if (!($id->usuario_id == Auth::user()->id)) {
                return redirect()->route('denied');
            }
            return $next($request);
        }
    }
}
