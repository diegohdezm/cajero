<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CuentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_cuenta_id' => 'required',
            'credito_saldo' => 'required_if:tipo_cuenta_id,1|numeric',
            'credito_aprobado' => 'required_if:tipo_cuenta_id,2|numeric',
            'comision_id' => 'required_if:tipo_cuenta_id,2',
        ];
    }

    /**gi
     * @return array
     */
    public function attributes(){
        return [
            'tipo_cuenta_id' => 'Tipo de cuenta',
            'credito_saldo' => 'Saldo inicial',
            'credito_aprobado' => 'Credito aprobado',
            'comision_id' => 'Comision'
        ];
    }
    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        if($validator->fails()) {
            throw new HttpResponseException(redirect()->back()->withErrors($validator));
        }
    }
}
