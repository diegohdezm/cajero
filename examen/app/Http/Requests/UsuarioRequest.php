<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:255',
            'a_paterno' => 'required|max:255',
            'a_materno' => 'required|max:255',
            'correo_electronico' => 'required|email|max:255',
            'edad' => 'required|numeric',
            'password' => 'required|max:255',
            'calle' => 'required|alpha_dash',
            'cp' => 'required|alpha_dash|max:15',
            'colonia' => 'required|alpha_dash',
            'extencion' => 'nullable|numeric',
            'prefijo' => 'nullable|numeric',
            'numero' => 'required|numeric',
        ];
    }

    /**gi
     * @return array
     */
    public function attributes(){
        return [
            'nombre' => 'Nombre',
            'a_paterno' => 'Apellido paterno',
            'a_materno' => 'Apellido materno',
            'correo_electronico' => 'Correo electronico',
            'edad' => 'Edad',
            'password' => 'Contraseña',
            'calle' => 'Calle',
            'cp' => 'Codigo postal',
            'colonia' => 'Colonia',
            'extencion' => 'Extencion',
            'prefijo' => 'Prefijo',
            'numero' => 'Numero'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        if($validator->fails()) {
            throw new HttpResponseException(redirect()->back()->withErrors($validator));
        }
    }
}
