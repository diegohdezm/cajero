<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ComisionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comision_retiro' => 'required|numeric|min:1',
        ];
    }

    /**gi
     * @return array
     */
    public function attributes(){
        return [
            'comision_retiro' => 'Comision retiro'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        if($validator->fails()) {
            throw new HttpResponseException(redirect()->back()->withErrors($validator));
        }
    }
}
