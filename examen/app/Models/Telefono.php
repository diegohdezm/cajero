<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Telefono extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'telefono';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'extencion', 'prefijo', 'numero',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
