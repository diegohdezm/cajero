<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Transaccion extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'transaccion';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'movimiento', 'cantidad',
        // Foraneas & Catalogos
        'usuario_id', 'cuenta_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Relaciones ===========================================
     */

    /**
     * Usuario de la transaccion (usuario_id).
     */
    public function usuario()
    {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    /**
     * Cuenta de la transaccion (cuenta_id).
     */
    public function cuenta()
    {
        return $this->belongsTo('App\Models\Cuenta', 'cuenta_id');
    }
}
