<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comision extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'comisiones';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'comision_retiro',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
