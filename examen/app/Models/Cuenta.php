<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Cuenta extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'cuenta';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'no_cuenta', 'credito_saldo', 'credito_aprobado', 'fecha_corte',
        // Catalogos
        'tipo_cuenta_id', 'comisiones_id', 'usuario_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Relaciones ===========================================
     */

    /**
     * Ususario de la direccion (usuario_id).
     */
    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }

    /**
     * Tipo de la cuenta (tipo_cuenta_id).
     */
    public function tipo_cuenta()
    {
        return $this->belongsTo('App\Models\TipoCuenta', 'tipo_cuenta_id');
    }

    /**
     * Comision de la cuenta (comisiones_id).
     */
    public function comision()
    {
        return $this->belongsTo('App\Models\Comision', 'comisiones_id');
    }
}
