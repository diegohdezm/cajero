<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Estado extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'estado';

    // Atributos
    protected $fillable = [
        'estado'
    ];
}
