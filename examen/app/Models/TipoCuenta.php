<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TipoCuenta extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'tpo_cuenta';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'tipo_cuenta',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
