<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Direccion extends Model
{
    // Traits
    use Notifiable;

    // Nombre de la tabla
    protected $table = 'direccion';

    /**
     * Atributos modificables
     *
     * @var array
     */
    protected $fillable = [
        'calle', 'cp', 'colonia',
        // Foraneas & Catalogos
        'telefono_id', 'estado_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Relaciones ===========================================
     */

    /**
     * Estado de la direccion (estado_id).
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\Estado', 'estado_id');
    }

    /**
     * Telefono de la direccion (telefono_id).
     */
    public function telefono()
    {
        return $this->belongsTo('App\Models\Telefono', 'telefono_id');
    }
}
