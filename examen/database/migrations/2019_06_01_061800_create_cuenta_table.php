<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_cuenta');
            $table->float('credito_saldo');
            $table->float('credito_aprobado')->nullable();
            $table->integer('fecha_corte')->nullable();
            $table->unsignedInteger('tipo_cuenta_id');
            $table->unsignedInteger('comisiones_id')->nullable();
            $table->unsignedInteger('usuario_id');
            $table->foreign('tipo_cuenta_id')->references('id')->on('tpo_cuenta');
            $table->foreign('comisiones_id')->references('id')->on('comisiones');
            $table->foreign('usuario_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta');
    }
}
