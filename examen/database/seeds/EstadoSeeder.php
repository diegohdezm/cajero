<?php

use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estado = [
            [
                'estado' => 'Mexico',
            ],
            [
                'estado' => 'CDMX',
            ],
            [
                'estado' => 'Hidalgo',
            ]
        ];

        DB::table('estado')->insert($estado);
    }
}
