<?php

use Illuminate\Database\Seeder;

class DireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $direccion = [
            [
                'calle' => 'Lago Zurich',
                'cp' => '9090',
                'colonia' => 'Ampliacion Granada',
                'telefono_id' => 1,
                'estado_id' => 1,
            ]
        ];

        DB::table('direccion')->insert($direccion);

        $oCuentabiente = \App\User::find(2);
        $oCuentabiente->direccion_id = 1;
        $oCuentabiente->save();
    }
}
