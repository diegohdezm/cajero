<?php

use Illuminate\Database\Seeder;

class TipoCuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_cuenta = [
            [
                'tipo_cuenta' => 'debito',
            ],
            [
                'tipo_cuenta' => 'credito',
            ]
        ];

        DB::table('tpo_cuenta')->insert($tipo_cuenta);
    }
}
