<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CuentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cuentas = [
            [
                'no_cuenta' => Str::random(15),
                'credito_saldo' => 8000,
                'credito_aprobado' => null,
                'fecha_corte' => null,
                'tipo_cuenta_id' => 1,
                'comisiones_id' => null,
                'usuario_id' => 2,
            ],
            [
                'no_cuenta' => Str::random(15),
                'credito_saldo' =>  25000,
                'credito_aprobado' => 25000,
                'fecha_corte' => date('d'),
                'tipo_cuenta_id' => 2,
                'comisiones_id' => 1,
                'usuario_id' => 2,
            ]
        ];

        DB::table('cuenta')->insert($cuentas);
    }
}
