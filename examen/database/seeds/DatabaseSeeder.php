<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TelefonoSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(DireccionSeeder::class);
        $this->call(ComisionSeeder::class);
        $this->call(TipoCuentaSeeder::class);
        $this->call(CuentaSeeder::class);
    }
}
