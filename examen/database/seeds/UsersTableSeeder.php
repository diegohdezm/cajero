<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'nombre' => Str::random(10),
                'tipo' => 'trabajador',
                'a_paterno' => Str::random(10),
                'a_materno' => Str::random(10),
                'email' => 'admin@admin.com',
                'edad' => 26,
                'status' => 1,
                'password' => bcrypt('secret'),
            ],
            [
                'nombre' => Str::random(10),
                'tipo' => 'cliente',
                'a_paterno' => Str::random(10),
                'a_materno' => Str::random(10),
                'email' => 'cliente@cliente.com',
                'edad' => 26,
                'status' => 1,
                'password' => bcrypt('secret'),
            ]
        ];

        DB::table('users')->insert($users);
    }
}
