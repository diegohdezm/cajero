<?php

use Illuminate\Database\Seeder;

class ComisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comisiones = [
            [
                'comision_retiro' => 10,
            ]
        ];

        DB::table('comisiones')->insert($comisiones);
    }
}
