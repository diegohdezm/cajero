<?php

use Illuminate\Database\Seeder;

class TelefonoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $telefono = [
            [
                'extencion' => '555',
                'prefijo' => '55',
                'numero' => '12345678',
            ]
        ];

        DB::table('telefono')->insert($telefono);
    }
}
